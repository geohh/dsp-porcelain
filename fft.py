from woof import fft as woof_fft
from dsp import signal

typecode_rfft_map = { \
        'float32' : 'cfloat32', \
        'uint16'  : 'cint16', \
        'int16'   : 'cint16', \
        }


def fft(signal_in):
    tcode = signal_in.typecode
    if signal_in.cplx:
        raise ValueError("finish writing woof already you fuck -hutch")
    else:
        signal_out = signal(typecode_rfft_map[tcode], len(signal_in))
        woof_fft.rfft(signal_out._data, signal_in._data)
    return signal_out


