import analogio
import arf

class asink():

    def _to_3MHZ(sampling_frequency):
        #ARF* currently exposes sampling periods as a unsigned int interpreted as a multiple of 1/3 us, so convert
        return math.trunc(3e+6/sampling_frequency)

    def __init__(self, pin, internal_class=arf.ARFOut, smapling_frequency=2000, in_type="uint16", gain=1, bias=0, usegb=False):
        self._sink = internal_class(pin)
        self._sink.sampling_period = asink._to_3MHz(sampling_frequency)
        self.sampling_frequency = sampling_frequency
        #Gain/bias support will be added later, I just wanted the function signature there for now.

    def write(self, signal):
        self._sink.send(signal._data)
        self.active = True

    def stop(self):
        self._sink.deinit()
        del(self._sink)


