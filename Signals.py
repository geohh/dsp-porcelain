import array
import arf

# Extended typecodes: 'cfloat32' - dummy complex via packed float32
#                     'cint32' - dummy complex via packed int32
#                     'cint16' - dummy complex via packed int16
# We reserve typecodes starting with 'c' to represent complex signals,
# so if support is added for 8-bit, it will be called [c][u]int8,
# not char

class Signal:
    typecode_map = {'cfloat32': 'f', \
                    'cint32': 'i', \
                    'cint16': 'h', \
                    'float32': 'f', \
                    'int32': 'i', \
                    'int16': 'h', \
                    'uint16': 'H', \
                    #extend as necessary
                     }


    def __init__(self, typecode, initializer):
        self.cplx = typecode[0] == 'c' #reserve typecode names starting with 'c' for complex-dummy
        self.typecode = typecode
        self._data = None
        if type(initializer) is int:
            self._data = array.array(self.typecode_map[typecode], [0 for _ in range((self.cplx + 1) * initializer)])
        else:
            self._data = array.array(self.typecode_map[typecode], initializer)

    def input(self, pin):
        self._input = arf.ARFIn(pin)

    def sync_read(self):
        self._input.recv(self._data)
        while self._input.dirty:
            pass

    def output(self, pin):
        self._output = arf.ARFOut(pin)

    def continuous_write(self):
        self._output.send(self._data)

    def __len__(self):
        if self.cplx:
            return len(self._data) // 2
        else:
            return len(self._data)

    def __getitem__(self, index):
        if type(index) is int:
            if self.cplx:
                return complex(self._data[2*index+0],self._data[2*index+1])
            else:
                return self._data[index]
        elif type(index) is slice:
            if index.step is None:
                return type(self)(self.typecode, self._data[index.start*self.cplx:index.stop*self.cplx])
            else:
                raise ValueError("Slicing with non-unit step not supported")

    def __setitem__(self, index, value):
        if self.cplx:
            self._data[2*index+0] = value.real
            self._data[2*index+1] = value.imag
        else:
            super()[index] = value

    def __str__(self):
        #Note: this is slow. Only use for debugging.
        if not self.cplx:
            return "Signal(\'" + self.typecode + "\', " + str(list(self._data)) + ")"
        else:
            return "Signal(\'" + self.typecode + "\', " + str([complex(self._data[2*i],self._data[2*i+1]) for i in range(len(self))]) + ")"


